using System.Net;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace src.Model
{
    public class ValidationFilterAttribute : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errorMessages = new StringBuilder();
                var serviceResult = new ServiceResult();
                serviceResult.IsSuccess = false;
                serviceResult.StatusCode = (int)HttpStatusCode.BadGateway;
                foreach (var modelState in context.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errorMessages.AppendLine(error.ErrorMessage);
                    }
                }
                serviceResult.Message = errorMessages.ToString();
                context.Result = new BadRequestObjectResult(serviceResult);
            }
        }
        public void OnActionExecuted(ActionExecutedContext context) { }
    }
}
