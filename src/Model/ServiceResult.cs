namespace src.Model
{
    public class ServiceResult
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public int StatusCode { get; set; }

        public object Data { get; set; }
    }
}