using FluentValidation;
using src.Model;

namespace src.Validations
{
    public class OrderValidation : AbstractValidator<Order>
    {
        public OrderValidation()
        {
            RuleFor(x => x.Id).NotEmpty();

            RuleFor(x => x.Name).NotNull();
        }
    }

    public class ParameterValidation : AbstractValidator<Parameter>
    {
        public ParameterValidation()
        {
            RuleFor(x => x.Key).NotEmpty();
        }
    }
}