using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using src.Model;

namespace src.Controllers
{
    /// <summary>
    /// Order Controller Desc
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        /// <summary>
        /// Post Sample
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Order")]
        public ActionResult Post([FromBody] Order order)
        {
            return Ok(new
            {
                reference = "test"
            });
        }

        /// <summary>
        /// Input List
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InputList")]
        public ActionResult Post([FromBody] List<Parameter> order)
        {
            return Ok(new
            {
                reference = "test"
            });
        }

    }
}